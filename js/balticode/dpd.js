function fillData($elementName, $callBackElementId)
{
    var x = document.getElementsByName($elementName);
    var r = document.getElementById($callBackElementId);
    var o = [];
    var i;
    for (i = 0; i < x.length; i++) {
        if (x[i].type == "checkbox" && x[i].checked == true) {
            o.push(x[i].value);
        }
    }
    var oJson = JSON.stringify(o);
    r.value = oJson;
    return true;
}
/*
 * Order list add js to show / hide block by ID
 */
function toggleWindow(objectId)
{
    $object = document.getElementById(objectId);
    if ($object.style.display == "block") {
        $object.style.display = "none";
    } else {
        $object.style.display = "block";
    }
}

/*
 * Save Options of carrier Method
 */
function saveShippingMethodOptions(url, quoteId, option)
{
    sendPost(url, 'quoteId='+quoteId+'&option=' + JSON.stringify(option));
}

/*
 * DPD send data action on Click
 * Change action and submit form
 */
function dpdSendData(form, action)
{
    var formId = form.parentNode.id;
    var form = document.getElementById(formId);
    form.action = action;
    form.submit();
}

/*
 * Ajax Function without jQuery
 */
function sendPost(url, params){
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", url, true);
    //Send the proper header information along with the request
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.setRequestHeader("Content-length", params.length);
    xmlhttp.setRequestHeader("Connection", "close");
    xmlhttp.onreadystatechange = function() {//Call a function when the state changes.
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        //<![CDATA[
        //    console.log(xmlhttp.responseText);
        //]]>
        }
    }
    xmlhttp.send(params);
}