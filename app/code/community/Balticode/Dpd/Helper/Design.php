<?php

class Balticode_Dpd_Helper_Design extends Mage_Core_Helper_Abstract
{
    /**
     * @return array
     */
    public function getAdminOrderReturnButton()
    {
        $params = array();
        if (Mage::helper('dpd')->getConfigData('active')) {
            $params = array(
                'label' => Mage::helper('dpd')->__('DPD Returns'),
                'onclick' => "toggleWindow('dpd_return_popup')",
            );
        }

        return $params;
    }

    /**
     * @return array
     */
    public function getAdminOrderLabelButton()
    {
        $params = array();
        if (Mage::helper('dpd')->getConfigData('active')) {
            $params = array(
                'label' => Mage::helper('dpd')->__('DPD Label'),
                'onclick' => "toggleWindow('dpd_label_popup')",
            );
        }

        return $params;
    }
}
