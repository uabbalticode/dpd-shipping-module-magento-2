<?php

class Balticode_Dpd_Helper_Datatype
{
    /**
     * @param $valiable
     * @return string
     */
    public function gettype($valiable)
    {
        $type = gettype($valiable);
        $result = $type;
        switch ($type) {
            case 'string':
                if ($this->is_Json($valiable)) {
                    $result = 'json';
                } elseif ($this->is_serialized($valiable)) {
                    $result = 'serialize';
                } elseif ($this->is_pdf($valiable)) {
                    $result = 'file';
                } elseif ($this->is_xml($valiable)) {
                    $result = 'xml';
                } elseif ($this->is_html($valiable)) {
                    $result = 'html';
                }
            break;
        }

        return $result;
    }

    /**
     * @param $content
     * @return bool|string
     */
    public function getFileType($content)
    {
        $result = false;
        // first of all check it is file?
        if ($this->gettype($content) == 'file') {
            if ($this->is_pdf($content)) {
                $result = 'pdf';
            }
        }

        return $result;
    }

    /**
     * test is JSON type string content
     *
     * @param  string  $string - some string
     * @return boolean
     */
    public function is_Json($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE); //It is no errors?
    }

    /**
     * test is PDF content
     *
     * @param  string  $fileContent PDF content
     * @return boolean
     */
    public function is_pdf($fileContent)
    {
        if (is_string($fileContent)) {
            $triggers = chr(37).chr(80).chr(68).chr(70).chr(45); //  %PDF-
            $heder = substr($fileContent, 0, strlen($triggers));
            if ($heder == $triggers) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * test is XML type string content
     *
     * @param  string  $string - some string
     * @return boolean
     */
    public function is_xml($content)
    {
        $doc = @simplexml_load_string($content);
        if ($doc) {
            return true; //this is valid
        } else {
            return false; //this is not valid
        }
    }

    /**
     * test is HTML type string content
     *
     * @param  string  $string - some string
     * @return boolean
     */
    public function is_html($content)
    {
        return preg_match("/<[^<]+>/", $content, $m) != 0;
    }

    /**
     * test is serialized type string content
     *
     * @param  string  $string - some string
     * @return boolean
     */
    public function is_serialized($data)
    {
        // if it isn't a string, it isn't serialized
        if (!is_string($data)) {
            return false;
        }

        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (!preg_match('/^([adObis]):/', $data, $badions)) {
            return false;
        }
        switch ($badions[1]) {
            case 'a' :
            case 'O' :
            case 's' :
                if (preg_match("/^{$badions[1]}:[0-9]+:.*[;}]\$/s",$data))
                    return true;
                break;
            case 'b' :
            case 'i' :
            case 'd' :
                if (preg_match("/^{$badions[1]}:[0-9.E-]+;\$/", $data)) {
                    return true;
                }
                break;
        }
        return false;
    }
}
