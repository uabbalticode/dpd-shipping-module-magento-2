<?php
/**
 * 2015 UAB BaltiCode
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License available
 * through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@balticode.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to
 * newer versions in the future.
 *
 *  @author    UAB Balticode Kęstutis Kaleckas
 *  @package   Balticode_DPD
 *  @copyright Copyright (c) 2015 UAB Balticode (http://balticode.com/)
 *  @license   http://www.gnu.org/licenses/gpl-3.0.txt  GPLv3
 */

class Balticode_Dpd_Adminhtml_ManifestController extends Balticode_Dpd_Controller_Adminhtml_Abstract
{
    /**
     * @param array $ordersIds
     * @return bool
     */
    public function ManifestAction($ordersIds = array())
    {
        //Collect Order id's
        $ordersIds = $this->getOrdersIds($ordersIds);
        //To Orders put Label Barcodes
        $availableOrders = $this->validateOrders($ordersIds); //select only available orders

        if ($availableOrders === false) { //If something wrong return false
            $this->goBack();
            return false;
        }

        $this->sortOrders($availableOrders);
        // Send data to API about printing Manifest
        $this->processParcelDataSend($availableOrders);
        $this->processManifestPrintPrint($availableOrders);

        if (0) { //This is for future
            $pdfContent = $this->getManifestFromAPI($availableOrders);
        } else {
            $pdfContent = $this->generateManifest($availableOrders); //Get Manifest PDF content
        }

        if (Mage::helper('dpd/datatype')->is_pdf($pdfContent)) {
            $this->_prepareDownloadResponse($this->getManifestFileName(), $pdfContent, 'application/pdf');
        } else {
            $this->goBack();
        }
    }

    /**
     * @param string $format
     * @return string
     */
    public function getDatetimeNow($format = 'Y-m-d') {
        $date_object = new DateTimeZone('Europe/Vilnius');
        $datetime = new DateTime();
        $datetime->setTimezone($date_object);

        return $datetime->format($format);
    }

    /**
     * Say to DPD WebLabel API about printing Manifest to need process current parcels
     *
     * @return boolean
     */
    private function processParcelDataSend($orders)
    {
        $manifestModel = Mage::getModel('dpd/api_ParcelDatasend');
        $manifestModel->collectOrders($orders);
        $manifestModel->setDateTime($this->getDatetimeNow());
        $manifestModel->collectData();
        $result = $manifestModel->send();

        return $result;
    }

    /**
     * Say to DPD WebLabel API about printing Manifest to need process current parcels
     *
     * @return boolean
     */
    private function processManifestPrintPrint($orders)
    {
        $manifestModel = Mage::getModel('dpd/api_manifest');
        $manifestModel->collectOrders($orders);
        $manifestModel->setDateTime($this->getDatetimeNow());
        $manifestModel->collectData();
        $result = $manifestModel->send();

        return $result;
    }

    /**
     * This function not full complete because DPD API not have full manifest report
     *
     * @param  array $availableOrders id_orders
     * @return boolean|string content from DPD API
     */
    private function getManifestFromAPI($availableOrders)
    {
        $availableOrders;
        $this->registerError('If you reality know what you doing so why you leave this message?');
        return false;

        // $parameters = array(
        //     'action' => 'parcel_manifest_print',
        //     'type' => 'manifest', // manifest, manifest_cod, summary_list
        //     'date' => date('Y-m-d'),
        // );

        // $api = Mage::getModel('dpd/api');
        // $pdfContent = $api->postData($parameters);
        // return $pdfContent;
    }

    /**
     * Return generated Manifest file name
     *
     * @param  array  $parameters some order parameters is for future
     * @return string Label file name
     */
    private function getManifestFileName()
    {
        $string = 'Manifest-';
        $time = date('Ymd_H-i', time());
        return $string.$time.'.pdf';
    }

    /**
     * Return Manifest PDF content
     *
     * @param  array  $availableOrders - Orders id
     * @return string - Manifest content
     */
    private function generateManifest($availableOrders = array())
    {
        require_once(str_replace('\\','/',Mage::getBaseDir().'/lib/dompdf/dompdf_config.inc.php'));

        $htmlContent = $this->getLayout()
            ->createBlock('dpd/adminhtml_sales_order_manifest', 'dpd_manifest')
            ->setOrders($availableOrders)
            ->toHtml();

        if (false) { //Preview If: true - Manifest show in internet browser; false - stream content to pdf
            echo $htmlContent;
        } else {
            $dompdf = new DOMPDF();
            $dompdf->load_html($htmlContent,"UTF-8");
            $dompdf->set_paper("A4", 'portrait');
            $dompdf->render();
            return $dompdf->output($this->getManifestFileName());
        }
    }

    /**
     * Sorter orders
     *
     * @param  pointer array &$availableOrders source array
     * @param  int array argument $direction - SORT_ASC to sort ascendingly or SORT_DESC to sort descendingly
     * @return null, sortered array is set to pointer
     */
    private function sortOrders(&$availableOrders, $direction = SORT_ASC)
    {
        if (!is_array($availableOrders)) {
            return false;
        }

        $sort_col = array();
        foreach ($availableOrders as $key => $id_order) {
            $sort_col[$key] = Mage::getModel('dpd/carrier')->getBarcodeFromOrder($id_order);
        }

        array_multisort($sort_col, $direction, $availableOrders);
    }

    /**
     * @return boolean
     */
    protected function _isAllowed()
    {
       return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/dpd/manifest');
    }
}
