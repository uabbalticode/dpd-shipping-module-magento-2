<?php
/**
 * 2016 UAB BaltiCode
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License available
 * through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@balticode.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to
 * newer versions in the future.
 *
 *  @author    UAB Balticode Kęstutis Kaleckas
 *  @package   Balticode_DPD
 *  @copyright Copyright (c) 2016 UAB Balticode (http://balticode.com/)
 *  @license   http://www.gnu.org/licenses/gpl-3.0.txt  GPLv3
 */

class Balticode_Dpd_Adminhtml_ReturnController
    extends Balticode_Dpd_Controller_Adminhtml_Abstract
{
    // private $orderIncrement = array();
    public function LabelAction()
    {
        $codes = array();
        $quantity = $this->collectPostData('Po_parcel_qty');
        $ordersBarcodes = $this->returnParcel('Return', array('packages' => $quantity));
        foreach ($ordersBarcodes as $order => $barcodes) {
            if (!is_array($barcodes)) {
                $codes[] = $barcodes;
            } else {
                foreach ($barcodes as $barcode) {
                    $codes[] = $barcode;
                }
            }
        }
        $pdfContent = Mage::getModel('dpd/api_Labels')->getPDF($codes);

        if (Mage::helper('dpd/datatype')->getFileType($pdfContent) == 'pdf') {
            $fileName = 'Return-label-'.implode('-', $this->orderIncrement).'.pdf';
            $this->_prepareDownloadResponse($fileName, $pdfContent, 'application/pdf');
        } else {
            Mage::helper('dpd')->registerError('error');
            $this->goBack();
        }
    }

    public function CollectionAction()
    {
        $quantity = $this->collectPostData('Po_parcel_qty');
        if (!$quantity) {
            $quantity = count($orderIds);
        }

        for ($i = 0; $i < $quantity; $i++) {
            $this->returnParcel('crImport');
        }

        $this->goBack();
    }

    /**
     * @param       $interface
     * @param array $params
     * @return array
     */
    protected function returnParcel($interface, $params = array())
    {
        $result = array();
        $orderIds = $this->collectOrders();

        $returnModel = Mage::getModel('dpd/api_'.$interface);

        $orders = $returnModel->collectOrders($orderIds)->getOrders();
        foreach ($orders as $key => $order) {
            $this->orderIncrement[] = $order->getIncrementId();
        }
        $returnModel->collectData($params);
        $result[] = $returnModel->send();

        return $result;
    }
}
