<?php
/**
 * 2015 UAB BaltiCode
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License available
 * through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@balticode.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to
 * newer versions in the future.
 *
 *  @author    UAB Balticode Kęstutis Kaleckas
 *  @package   Balticode_DPD
 *  @copyright Copyright (c) 2016 UAB Balticode (http://balticode.com/)
 *  @license   http://www.gnu.org/licenses/gpl-3.0.txt  GPLv3
 */

class Balticode_Dpd_Adminhtml_LabelController
    extends Balticode_Dpd_Controller_Adminhtml_Abstract
{
//     private $dpd_available_parcel_types = array(
//         array('service_code' => '803',
//             'servce_description' => 'Parcel Shop',
//             'parcel_type' => 'PS',
//             'service_elements' => '601',
//             'service_mark' => '',
//             'service_text' => 'PS'),

//         array('service_code' => '329',
//             'servce_description' => 'Normal Parcel, COD, B2C',
//             'parcel_type' => 'D-COD-B2C',
//             'service_elements' => '001,013,100',
//             'service_mark' => '',
//             'service_text' => 'D-COD-B2C'),

//         array('service_code' => '327',
//             'servce_description' => 'Normal Parcel, B2C',
//             'parcel_type' => 'D-B2C',
//             'service_elements' => '1,013',
//             'service_mark' => '',
//             'service_text' => 'D-B2C'),
//         );

//     public $errorMessages = array('error' => array(), 'warning' => array());

//     private $orderIncrement = array();

    /**
     * Return DPD Label from API if Available
     *
     * @param  array of parameters important is orders id
     * @return mix if false some error is, else go to Barcode PDF content
     */
    public function LabelAction($ordersIds = array(), $parcelQty = null)
    {
        $this->getLabels($ordersIds, 'join');
    }

    /**
     * @param array $ordersIds
     * @param null  $parcelQty
     */
    public function LabelMpsAction($ordersIds = array(), $parcelQty = null)
    {
        $this->getLabels($ordersIds, 'split');
    }

    /**
     * @param array $ordersIds
     */
    public function ReturnAction($ordersIds = array())
    {
        $this->getLabels($ordersIds, 'join', true);
    }

    /**
     * @param array $ordersIds
     */
    public function returnMpsAction($ordersIds = array())
    {
        $this->getLabels($ordersIds, 'split', true);
    }

    /**
     * @param      $ordersIds
     * @param null $parcelQty
     * @param bool $needReturn
     * @return bool|void
     */
    public function getLabels($ordersIds, $parcelQty = null, $needReturn = false)
    {
        //Collect Order id's
        $ordersIds = $this->getOrdersIds($ordersIds);
        //To Orders put Label Barcodes
        $availableOrders = $this->validateOrders($ordersIds); //select only available orders

        if ($availableOrders == false) {
            $this->goBack(); //return
            return false;
        }

        $quantity = $this->collectPostData('Po_parcel_qty');
        if ($quantity > 1) {
            $parcelQty = $quantity;
        }

        $this->generateBarcode($availableOrders, $parcelQty); //Generate Barcodes from available orders

        $returnBarcodes = array();
        if ($needReturn) { //if need send data about return and get barcodes
            $returnModel = Mage::getModel('dpd/api_Return');
            $returnModel->collectOrders($availableOrders);
            $returnModel->collectData();
            $returnBarcodesResponse = $returnModel->sendCollection();

            foreach ($returnBarcodesResponse as $orderline => $orderBarcodes) {
                foreach ($orderBarcodes as $barcode) {
                    $returnBarcodes[] = $barcode;
                }
            }
        }
        //Collect barcodes from orders
        $orderBarcodes = Mage::getModel('dpd/carrier')->getBarcodeByOrdersUnique($availableOrders);

        //Merge return barcodes with labels barcodes
        $barcodes = array_merge($orderBarcodes, $returnBarcodes);
        //Send data to API request labels pdf file

        $labelsModel = Mage::getModel('dpd/api_Labels');
        $labelsModel->collectOrders($availableOrders);
        $labelsModel->collectData();
        $pdfContent = $labelsModel->getPDF($barcodes);

        if ($pdfContent == false) {
            $this->goBack();
            return;
        }

        if (Mage::helper('dpd/datatype')->is_pdf($pdfContent)) {
            $this->_prepareDownloadResponse($this->getLabelFileName(), $pdfContent, 'application/pdf');
        } else {
            $this->registerError(Mage::helper('dpd')->__('Error: DPD API return not PDF file format'));
            $this->goBack();
        }
    }

    /**
     * Return generated Labels file name
     *
     * @param  array  $parameters some order parameters is for future
     * @return string Label file name
     */
    private function getLabelFileName($parameters = array())
    {
        $parameters; //This is for validation;
        $staticString = 'Labels-';
        $time = date('Ymd_H-i', time());
        return $staticString.$time.'.pdf';
    }

    /**
     * Generate of Barcode, Send data about order to DPD API
     * Grab errors or Barcode number jSon format
     *
     * @param  array - all available orders objects
     * @return mixed
     */
    private function generateBarcode($availableOrders, $labelType)
    {
        if (!count($availableOrders)) { //If no orders id found return false
            return false;
        }

        $differenceOrders = $this->groupOrders($availableOrders, $labelType); //group orders this for MPS method

        $api = Mage::getModel('dpd/api');
        $carrier = Mage::getModel('dpd/carrier');
        //Generate Barcodes by single orders
        foreach ($differenceOrders['single'] as $customerAddressId => $orderTypes) {
            foreach ($orderTypes as $orderType => $orders) {
                foreach ($orders as $order) {
                    if ($carrier->hasBarcode($order)) {
                        continue;
                    }
                    $method = $carrier->getMethod($order->getShippingMethod());
                    $carrierMethod = $carrier->getModelClass($method);
                    $returnDetails = $carrierMethod->returnDetails($orders, $labelType);

                    if (!is_array($returnDetails)) {
                        $this->registerError(Mage::helper('dpd')->__('Lost some data to format DataSend array'));
                        return false;
                    }

                    $apiModel = Mage::getModel('dpd/api_ParcelImport');
                    $apiModel->collectOrders($order);
                    $apiModel->collectData($returnDetails);
                    $apiReturn = $apiModel->send(false);

                    $apiReturn = json_decode($apiReturn);

                    if ($apiReturn === false) {
                        foreach ($api->getErrorMessages() as $errorMessage) {
                            $carrier->addMessageToOrder($order, $errorMessage, 'DPD ERROR: ');
                            $this->registerError($errorMessage);
                        }
                        return false;
                    }
                    if ($apiReturn->status == 'ok') {
                        if (!empty($apiReturn->errlog)) {
                            Mage::helper('dpd')->registerWarning($apiReturn->errlog);
                        }
                        foreach ($apiReturn->pl_number as $barcode) {
                            if (empty($barcode)) {
                                $errorMessage = Mage::helper('dpd')->__('Barcode is empty');
                                $carrier->addMessageToOrder($order, $errorMessage, 'DPD ERROR: ');
                            } else {
                                $carrier->setBarcodeToOrder($order, $barcode);
                            }
                        }
                    } elseif ($apiReturn->status == 'err') {
                        if (!empty($apiReturn->errlog)) {
                            Mage::helper('dpd')->registerWarning($apiReturn->errlog);
                        }
                    }
                }
            }
        }
        //Generate Barcodes by multiple orders -> MPS (join)
        foreach ($differenceOrders['multi'] as $customerAddressId => $orderTypes) {
            foreach ($orderTypes as $orderType => $orders) {
                $order = reset($orders);
                $method = $carrier->getMethod($order->getShippingMethod());
                $carrierMethod = $carrier->getModelClass($method);
                $returnDetails = $carrierMethod->returnDetails($orders, $labelType);
                if (!is_array($returnDetails)) {
                    $this->registerError(Mage::helper('dpd')->__('Lost some data to format DataSend array'));
                    return false;
                }
                //$apiReturn = $api->postData($returnDetails); //Get data form DPD API
                $helper = Mage::getModel('dpd/api_ParcelImport', array($order));
                $helper->collectData($returnDetails);

                $apiReturn = $helper->send(false);
                $apiReturn = json_decode($apiReturn);
                if ($apiReturn === false) {
                    foreach ($api->getErrorMessages() as $errorMessage) {
                        $this->registerError($errorMessage);
                        foreach ($orderTypes as $orders) {
                            foreach ($apiReturn->pl_number as $barcode) {
                                foreach ($orders as $order) {
                                    $carrier->addMessageToOrder($order, $errorMessage, 'DPD ERROR: ');
                                }
                            }
                        }
                    }
                    return false;
                }
                if ($apiReturn->status == 'ok') {
                    if (!empty($apiReturn->errlog)) {
                        Mage::helper('dpd')->registerWarning($apiReturn->errlog);
                    }

                    foreach ($orderTypes as $orders) {
                        foreach ($apiReturn->pl_number as $barcode) {
                            foreach ($orders as $order) {
                                $carrier->setBarcodeToOrder($order, $barcode);
                            }
                        }
                    }
                }
            }
        }
        //Generate Barcodes by orders -> MPS (split)
        foreach ($differenceOrders['mps'] as $customerAddressId => $orderTypes) {
            $total_num_of_parcels = 0;
            foreach ($orderTypes as $orders) {
                foreach ($orders as $order) {
                    $total_num_of_parcels += count($order->getAllVisibleItems());
                }
            }

            foreach ($orderTypes as $orderType => $orders) {
                $order = reset($orders);
                $method = $carrier->getMethod($order->getShippingMethod());
                $carrierMethod = $carrier->getModelClass($method);
                $returnDetails = $carrierMethod->returnDetails($orders, $labelType);

                if (!is_array($returnDetails)) {
                    $this->registerError(Mage::helper('dpd')->__('Lost some data to format DataSend array'));
                    return false;
                }

                //$apiReturn = $api->postData($returnDetails); //Get data form DPD API
                $helper = Mage::getModel('dpd/api_ParcelImport', array($order));
                $helper->collectData($returnDetails);
                $apiReturn = $helper->send(false);

                $apiReturn = json_decode($apiReturn);

                if ($apiReturn === false) {
                    foreach ($api->getErrorMessages() as $errorMessage) {
                        $this->registerError($errorMessage);
                        foreach ($orderTypes as $orders) {
                            foreach ($orders as $order) {
                                $carrier->addMessageToOrder($order, $errorMessage, 'DPD ERROR: ');
                            }
                        }
                    }
                    return false;
                }
                if ($apiReturn->status == 'ok') {
                    if (!empty($apiReturn->errlog)) {
                        Mage::helper('dpd')->registerWarning($apiReturn->errlog);
                    }

                    $barcodes = (array)$apiReturn->pl_number;
                    if ($total_num_of_parcels == count($barcodes)) { //if i got correct quantity barcodes
                        $i = 0;
                        foreach ($orderTypes as $orders) {
                            foreach ($orders as $order) {
                                foreach ($order->getAllVisibleItems() as $product) {
                                    $carrier->setBarcodeToOrder($order, $barcodes[$i++]);
                                }
                            }
                        }
                    } else {
                        foreach ($apiReturn->pl_number as $barcode) {
                            foreach ($orderTypes as $orders) {
                                foreach ($orders as $order) {
                                    $carrier->setBarcodeToOrder($order, $barcode);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Group orders who not have Barcodes
     * This is need for MPS method
     *
     * @param  array - Orders who need group
     * @return array - Multidimensional array
     *         array(
     *             'single' => array([orders]), //Single order not grouping
     *             'multi' => array(
     *                  'groups'=> array([order])), //Grouped orders
     *             'given' => array([orders]), //order list who already have barcode in messages
     *         )
     */
    public function groupOrders($ordersObj = array(), $parcelQty)
    {
        if (!is_array($ordersObj)) { //if not array set
            $ordersObj = array($ordersObj);
        }

        if (!count($ordersObj)) { //if orders array is empty
            return $groupedOrders; //return empty array
        }
        //MPS grouping data
        // MPS available when:
        //  Same Shipping Address;
        //  Same Package Type;
        //  Same Delivery Day;
        //  Order Delivery NOT to ParcelStore

        $groupedOrders = array(
            'single' => array(),
            'given' => array(),
            'multi' => array(),
            'mps' => array()
        );

        $orders = array();

        foreach ($ordersObj as $order) {
            if (Mage::getModel('dpd/carrier')->hasBarcode($order)) { //This order already has barcode?
                $groupedOrders['given'][] = $order;
                continue;
            }

            $customerAddressId = $order->getShippingAddress()->getCustomerAddressId();
            if (empty($customerAddressId)) {
                $customerAddressId = $order->getBillingAddress()->getCustomerAddressId();
            }
            if (empty($customerAddressId)) {
                $customerAddressId = $order->getShippingAddressId();
            }

            $orders[$customerAddressId][$this->getParcelType($order)][] = $order;
        }

        if ($parcelQty == 'split') { // We need to split order by item to create MPS
            foreach ($orders as $customerAddressId => $parcelTypes) {
                foreach ($parcelTypes as $parcelType => $orders) {
                    foreach ($orders as $order) {
                        if ($parcelType == 'PS') {
                            $groupedOrders['single'][$customerAddressId][$parcelType][] = $order;
                        } else {
                            $order->addData(array('mpsParcelQty' => $this->getParcelQuantity($order, $parcelQty)));
                            $groupedOrders['mps'][$customerAddressId][$parcelType][] = $order;
                        }
                    }
                }
            }
        } elseif ($parcelQty == 'join') { //We need to join order to create one label
            foreach ($orders as $customerAddressId => $parcelTypes) {
                foreach ($parcelTypes as $parcelType => $orders) {
                    if (count($orders) > 1) {
                        foreach ($orders as $order) {
                            if ($parcelType == 'PS') {
                                $groupedOrders['single'][$customerAddressId][$parcelType][] = $order;
                            } else {
                                $groupedOrders['multi'][$customerAddressId][$parcelType][] = $order;
                            }
                        }
                    } else {
                        $groupedOrders['single'][$customerAddressId][$parcelType][] = reset($orders);
                    }
                }
            }
        } elseif (is_numeric($parcelQty) && count($orders) == 1) {
            foreach ($orders as $customerAddressId => $parcelTypes) {
                foreach ($parcelTypes as $parcelType => $orders) {
                    $order = reset($orders);
                    if ($parcelType == 'PS') {
                        $groupedOrders['single'][$customerAddressId][$parcelType][] = $order;
                    } else {
                        $order->addData(array('mpsParcelQty' => $this->getParcelQuantity($order, $parcelQty)));
                        $groupedOrders['mps'][$customerAddressId][$parcelType][] = $order;
                    }
                }
            }
        }

        return $groupedOrders;
    }

    /**
     * @return boolean
     */
    protected function _isAllowed()
    {
       return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/dpd/labels');
    }
}
