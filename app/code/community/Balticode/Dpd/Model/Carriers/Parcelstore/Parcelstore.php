<?php
/**
 * 2015 UAB BaltiCode
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License available
 * through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@balticode.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to
 * newer versions in the future.
 *
 *  @author    UAB Balticode Kęstutis Kaleckas
 *  @package   Balticode_DPD
 *  @copyright Copyright (c) 2015 UAB Balticode (http://balticode.com/)
 *  @license   http://www.gnu.org/licenses/gpl-3.0.txt  GPLv3
 */

class Balticode_Dpd_Model_Carriers_Parcelstore_Parcelstore
    extends Balticode_Dpd_Model_Carriers_Carriers
    implements Mage_Shipping_Model_Carrier_Interface
{
    /**
     * Maximum weight shipping of this carrier
     * @var int
     */
    public $maxWeight = 20;

    /**
     * cm (any side)
     * @var int
     */
    public $maxLength = 100;

    /**
     * cm
     * @var int
     */
    public $maxScope = 200;

    /**
     * @var string
     */
    public $key = 'ps';

    /**
     * @var string
     */
    protected $_code = 'balticode_dpd';

    /**
     * Number is using when client is old and parcel Number calculation by one
     * So set this number to continue count
     * @var integer
     */
    private $addictional_order_number = 0;

    /**
     * Return available carrier methods
     *
     * @return array list of carriers
     */
    public function getAllowedMethods()
    {
        return array(
            'standard'     =>  __('Parcel Store'),
        );
    }

    /**
     * Collect using data and return
     *
     * @param $parameters
     * @return array
     */
    public function collectParams($parameters)
    {
        $return = array();
        if (isset($parameters['dpd_delivery_id'])) {
            $return[$this->key]['dpd_delivery_id'] = $parameters['dpd_delivery_id'];
        }
        if (isset($parameters['dpd_delivery_comment'])) {
            $return[$this->key]['dpd_delivery_comment'] = $parameters['dpd_delivery_comment'];
        }
        if (!count($return[$this->key])) {
            $return[$this->key]['all'] = $parameters;
        }
        return $return;
    }

    /**
     * Create description of carrier
     *
     * @param  string $currentDescription some text already Write of parent class
     * @return string                     new text for this description
     */
    public function getShippingDescription($orderId, $currentDescription = '')
    {
        $parcelStoreId = $this->getParcelStoreIdByOrder($orderId);

        if (isset($parcelStoreId['delivery_id'])) {
            $parcelStore = Mage::getModel('dpd/deliverypoints')
                ->getDeliveryPoints(false, false, $parcelStoreId['delivery_id']);
            $parcelStore = array_values($parcelStore);
            $parcelStore = array_values($parcelStore[0]);
            $label = 'Selected Parcel Store: ';
            $psName = $parcelStore[0]['city'].' - '
                    .$parcelStore[0]['company']
                    .' '.$parcelStore[0]['street']
                    .' '.$parcelStore[0]['country']
                    .'-'.$parcelStore[0]['pcode']
                    .' '.$parcelStore[0]['phone'];
            $psName = preg_replace('!\s+!', ' ', $psName); //Multi space replace to single space

            return $currentDescription.' / '.Mage::helper('dpd')->__($label).$psName;
        }

        if (isset($parcelStoreId['delivery_comment']) && strlen($parcelStoreId['delivery_comment'])) {
            return $currentDescription.' ('.htmlentities($parcelStoreId['delivery_comment']).')';
        }

        return $currentDescription;
    }

    /**
     * Load order and get option of dpdDeliveryOption
     *
     * @param  string $orderId
     * @return string          delivery parcelStore id
     */
    public function getParcelStoreIdByOrder($order)
    {
        if (is_numeric($order)) {
            $order = Mage::getModel('sales/order')->load($order);
        }
        $deliveryOptionSerialize = $order->getDpdDeliveryOptions();
        if ($deliveryOptionSerialize === null) {
            return false;
        }
        $deliveryOptionArray = unserialize($deliveryOptionSerialize);
        $optionKey = array_keys($deliveryOptionArray); //classic or PS

        if ($optionKey[0] == $this->key) {

            if (isset($deliveryOptionArray[$this->key]['dpd_delivery_id'])) {
                return array('delivery_id' => $deliveryOptionArray[$this->key]['dpd_delivery_id']);
            }

            if (isset($deliveryOptionArray[$this->key]['dpd_delivery_comment'])) {
                return array('delivery_comment' => $deliveryOptionArray[$this->key]['dpd_delivery_comment']);
            }
        }

        return false;
    }

    /**
     * Return type of method it is using for API request
     *
     * @param  [type] $orderId [description]
     * @return [type]          [description]
     */
    public function getType($orderId)
    {
        if ($this->isCodMethod($orderId)) {
            return 'PSCOD';
        } else {
            return 'PS';
        }
    }

    /**
     * Return Available Payment Method for order
     *
     * @param  object | Order
     * @return boolean
     */
    public function availablePaymentMethod($order)
    {
        $quoteId = $order->getData('quote_id');
        $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($quoteId);
        return $this->isCodAvailable($quote);
    }

    /**
     * Create array with parameters who need to send to API
     *
     * @param $orders       object $ordersId order
     * @param null $labelType
     * @return array|bool   description of orders
     */
    public function returnDetails($orders = array(), $labelType = null)
    {
        if (count($orders) > 1) {
            Mage::helper('dpd')->log('ParcelStore can be only Single (non MPS). Got '.count($orders).' Orders');
        }
        $order = reset($orders);
        $shippingAddress = $order->getShippingAddress();
        $parcelStoreId = $this->getParcelStoreIdByOrder($order);
        if ($parcelStoreId === false) {
            return false;
        }
        $parcelStoreId = $parcelStoreId['delivery_id'];
        $parcelStore = array();
        $parcelStore = Mage::getModel('dpd/deliverypoints')->getDeliveryPoints(false, false, $parcelStoreId);
        $parcelStore = array_values($parcelStore);
        $parcelStore = array_values($parcelStore[0]);
        $returnDetails = array(
            'name1' => $shippingAddress->getName(),
            'name2' => $parcelStore[0]['company'],
            'street' => $parcelStore[0]['street'],
            'pcode' => preg_replace('/^\D{2}([ -]?)(?=\d{4,5})/', '', $parcelStore[0]['pcode']),
            'country' => strtoupper($parcelStore[0]['country']),
            'city' => $parcelStore[0]['city'],
            'phone' => $shippingAddress->getTelephone(),
            'parcelshop_id' => $parcelStoreId,
        //    'remark' => $this->_getRemark($order),
        //    'Po_type' => $this->getConfigData('senddata_service'),
            'num_of_parcel' => '1',
            'order_number' => str_pad((int)$order->getIncrementId() + $this->addictional_order_number, 10, '0', STR_PAD_LEFT),
            'idm' => 'Y', //Parcelshop is required the idm parameters
            'idm_sms_rule' => 902, //Write the sum amount of the chosen SMS rules:
                            // 1 – pickup               0b1000000
                            // 2 – non delivery     0b0100000
                            // 4 – delivery         0b0010000
                            // 8 – inbound              0b0001000
                            // 16 – out for delivery    0b0000100
                            // 902 (when using PS type, then the value MUST be );
            'parcel_type' => (string)$this->getType($order->getId()),
            'action' => 'parcel_import',
        );

        if ($this->isCodMethod($order->getId())) {
            $ordersPrice = (float)Mage::helper('dpd/data')
                ->convertCurrency((float)$order->getBaseGrandTotal());
            $returnDetails['cod_amount'] = (float)round($ordersPrice,2);
        }

        return $returnDetails;
    }

    /**
     * Is enabled this method
     *
     * @return boolean
     */
    public function isEnabled($storeId = null)
    {
        $enabled = Mage::helper('dpd/data')->getConfigData('parcelstore_enabled', $storeId) //This method
            & Mage::helper('dpd/data')->getConfigData('active', $storeId); //Global

        if (!Mage::getModel('dpd/deliverypoints')->getDeliveryPoints()) {
            $enabled = false;
        }

        return $enabled;
    }

    /**
     * @param $_code
     * @param $request
     * @return bool|Mage_Shipping_Model_Rate_Result_Method
     */
    public function getShippingRateResult($_code, $request)
    {
        $quote = $request['all_items'][0]->getQuote();
        $quote = Mage::getModel('sales/quote')->loadByIdWithoutStore($quote->getId());
        $storeId = $request->getStoreId();

        if ($request->getFreeShipping() === true) {
            $price = '0.00';
        } else {
            $price = $this->getPrice($quote, $storeId);
        }

        if ($price === false) {//Method not available for this quote
            return false;
        }

        if (Mage::helper('dpd/data')->getConfigData('parcelstore_sallowspecific', $storeId)) {//Specific Country
            $country = explode(',', Mage::helper('dpd/data')->getConfigData('parcelstore_specificcountry', $storeId));
            if (count($country)) {
                if (method_exists($quote, 'getShippingAddress')) {
                    if (!in_array($quote->getShippingAddress()->getCountryId(), $country)) {
                        return false;
                    }
                }
            } else {
                return false; //Nothing select
            }
        }

        /** @var Mage_Shipping_Model_Rate_Result_Method $rate */
        $rate = Mage::getModel('shipping/rate_result_method');
        $rate->setCarrier($_code);
        $rate->setMethod($this->key);
        $rate->setCarrierTitle('DPD');
        $rate->setMethodTitle(Mage::helper('dpd/data')->getConfigData('parcelstore_title', $storeId));
        $rate->setPrice($price);
        $rate->setCost($price);

        return $rate;
    }

    /**
     * Get Price to this shipping method
     *
     * @param  object $quote    Current Quote
     * @param  int    $storeId
     * @return mixed boolean - false if not available
     *         Float data of shipping price
     */
    public function getPrice($quote, $storeId = null)
    {
        if ($storeId == null) {
            $storeId = $quote->getStoreId();
        }

        $totalCartWeight = (float)$quote->getShippingAddress()->getWeight();
        //Order is to heavy
        if ($totalCartWeight > (float)$this->maxWeight) {
            return false;
        }

        $cartProductDimensions = $this->getCartProductsDimensions($quote);
        if (!count($cartProductDimensions['height'])
            || !count($cartProductDimensions['width'])
            || !count($cartProductDimensions['depth'])) {
                return (float)Mage::helper('dpd/data')->getConfigData('parcelstore_price', $storeId);
            }

        $cartHeight = max($cartProductDimensions['height']);
        $cartWidth = max($cartProductDimensions['width']);
        $cartDepth = max($cartProductDimensions['depth']);
        $cartScope = ($cartHeight + $cartWidth) * 2 + $cartDepth;

        //Order is to Big
        if ((max($cartHeight, $cartWidth, $cartDepth) > $this->maxLength)
            || $cartScope > $this->maxScope) {
            return false;
        }

        if ((boolean)Mage::helper('dpd/data')->getConfigData('parcelstore_free_enable', $storeId)) {//Enabled free shipping?
            if ((float)$quote->getSubtotal()
                >= (float)Mage::helper('dpd/data')->getConfigData('parcelstore_free_subtotal', $storeId)) {
                return (float)0.0;
            }
        }

        return (float)Mage::helper('dpd/data')->getConfigData('parcelstore_price', $storeId);
    }

    /**
     * Return cart products with dimensions
     *
     * @param  quote (object)
     * @return array
     */
    public function getCartProductsDimensions($quote)
    {
        $dimensions = array(
            'id_product' => array(),
            'quantity' => array(),
            'height' => array(),
            'width' => array(),
            'depth' => array(),
            'diagonal' => array(),
        );

        //$quoteProducts = $quote->getAllItems();
        //$catalogProduct = Mage::getModel('catalog/product');

        $returnItems = array();
        foreach ($quote->getAllVisibleItems() as $item) {
            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $returnItems[] = $child;
                }
            } else {
                $returnItems[] = $item;
            }
        }

        foreach ($returnItems as $key => $currentProduct) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $currentProduct->getSku());
            $dimensions['id_product'][$key] = (int)$product->getId();
            $dimensions['quantity'][$key] = (float)$currentProduct->getQty();
            $dimensions['height'][$key] = (float)$product->getPackageHeight();
            $dimensions['width'][$key] = (float)$product->getPackageWidth();
            $dimensions['depth'][$key] = (float)$product->getPackageDepth();
            $dimensions['diagonal'][$key] = (float)$currentProduct->getQty()
                * $this->getDiagonal((float)$product->getPackageHeight(),
                    (float)$product->getPackageWidth(),
                    (float)$product->getPackageDepth()
                );
        }
        return $dimensions;
    }

    /**
     *
     *                     Diagonal
     *                       |
     *                  *****|**********
     *                *.\    |       * *
     *              *  . \ <-|     *   *
     *            *    .  \      *     *
     *           ****************      *    <- Height
     *           *     .    \   *      *
     *           *     ......\..*......*
     *           *   .        \ *    *
     *           * .           \*  *    <- Depth
     *           ****************
     *                 Width
     *
     *
     * @param $height
     * @param $width
     * @param $depth
     * @return float
     */
    public function getDiagonal($height, $width, $depth)
    {
        $height = (float)trim($height);
        $width = (float)trim($width);
        $depth = (float)trim($depth);
        $dimensions = sqrt(($height * $height) + ($width * $width) + ($depth * $depth));
        return $dimensions;
    }

    /**
     * Return data about this shipping method (price, name, etc.)
     *
     * @param  Mage_Shipping_Model_Rate_Request $request
     * @return object Mage_Core_Shipping_Model_Rate_Result with parameters
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        $result = Mage::getModel('shipping/rate_result');

        $result->append($this->getShippingRateResult());
        return $result;
    }

    public function isCodAvailable($quote)
    {
        $parcelshop_id = null;
        // If value come from checkout form
        if (isset($_POST['shipping_method']) && isset($_POST['dpd_delivery_id'])) {
            if ($_POST['shipping_method'] == 'dpd_'.$this->key) {
                $parcelshop_id = $_POST['dpd_delivery_id'];
            }
        }

        if (empty($parcelshop_id)) {
            $quote = Mage::getModel('sales/quote')
                ->loadByIdWithoutStore($quote->getId());

            $dpdDeliveryOptions = unserialize($quote->getDpdDeliveryOptions());
            $parcelshop_id = $dpdDeliveryOptions[$this->key]['dpd_delivery_id'];
        }

        $deliveryPoints = Mage::getModel('dpd/deliverypoints')->collectDeliveryPoints(false, false, $parcelshop_id);
        // If found only one line
        if (count($deliveryPoints) == 1) {
            $deliveryPoint = reset($deliveryPoints);
            return $deliveryPoint['cod'];
        }

        return false;
    }
}
