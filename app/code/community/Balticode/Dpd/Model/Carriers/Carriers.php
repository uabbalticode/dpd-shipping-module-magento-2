<?php
/**
 * 2015 UAB BaltiCode
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License available
 * through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@balticode.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to
 * newer versions in the future.
 *
 *  @author    UAB Balticode Kęstutis Kaleckas
 *  @package   Balticode_DPD
 *  @copyright Copyright (c) 2018 UAB Balticode (http://balticode.com/)
 *  @license   http://www.gnu.org/licenses/gpl-3.0.txt  GPLv3
 */

class Balticode_Dpd_Model_Carriers_Carriers
    extends Mage_Shipping_Model_Carrier_Abstract
{
    /**
     * List of payment methods who is cache on delivery, this using for generate parcel type
     *
     * @var array
     */
    public $cod_methods = array(
        'Cash On Delivery' => 'cashondelivery',
        'Balticode Cash On Delivery' => 'Balticode_Cashondelivery'
    );

    /**
     * Return is Cash On Delivery Payment Method of this order
     *
     * @param  int  $order_id
     * @return boolean
     */
    public function isCodMethod($order_id)
    {
        $paymentCode = Mage::helper('dpd/data')->getPaymentCode($order_id);
        return in_array($paymentCode, $this->cod_methods);
    }

    /**
     * Return Available Payment Method for order
     *
     * @param  object | Order
     * @return boolean
     */
    public function availablePaymentMethod($order)
    {
        return true;
    }

    public function isCodAvailable($quote)
    {
        return true;
    }

   /**
     * Return data about this shipping method (price, name, etc.)
     *
     * @param  Mage_Shipping_Model_Rate_Request $request
     * @return object Mage_Core_Shipping_Model_Rate_Result with parameters
     */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        $result = Mage::getModel('shipping/rate_result');
        // $result->append($this->getShippingRateResult());
        return $result;
    }
}
