<?php

class Balticode_Dpd_Model_Api_Manifest extends Balticode_Dpd_Model_Api_Abstract
{
    /**
     * @var string
     */
    protected $interface = 'parcel_manifest_print.php';
}
