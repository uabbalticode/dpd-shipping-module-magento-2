<?php

class Balticode_Dpd_Model_Api_Return extends Balticode_Dpd_Model_Api_Abstract
{
    /**
     * @var string
     */
    protected $interface = 'parcel_import.php';

    /**
     * @var array
     */
    protected $parameters = array(
        'username' => '', //* client's weblabel username
        'password' => '', //* client's weblabel password
        'name1' => '',
        'street' => '',
        'city' => '',
        'country' => '',
        'pcode' => '',
        'weight' => '',
        'num_of_parcel' => 0,
        'parcel_type' => 'D-Return',
        'return_service_only' => 'Y',
    );

    /**
     * @return $this
     */
    protected function fillParameters()
    {
        parent::fillParameters(); //Fill with user and password
        $shippingAddress = $this->order->getShippingAddress();

        $this->parameters['name1'] = $shippingAddress->getFirstname();
        $this->parameters['street'] = $shippingAddress->getData('street');
        $this->parameters['city'] = $shippingAddress->getCity();
        $this->parameters['country'] = $shippingAddress->getCountryId();
        $this->parameters['pcode'] = preg_replace('/^\D{2}([ -]?)(?=\d{4,5})/', '', $shippingAddress->getPostcode());
        $this->parameters['weight'] = (float)$shippingAddress->getOrder()->getWeight();
        $this->parameters['num_of_parcel'] = $this->getParcelNumber();

        return $this;
    }

    /**
     * @return int|string
     */
    private function getParcelNumber()
    {
        $quantity = 1;
        $params = $this->_params;
        if (isset($params['packages'])) {
            $quantity = $params['packages'];
        } elseif (isset($params['type']) && $params['type'] == 'mps') {
            $mpsType = Mage::helper('dpd/data')->getConfigData('mps_type');

            if ($mpsType == 'item') {
                $quantity = count($this->order->getAllVisibleItems());
            } elseif ($mpsType == 'quantity') {
                $quantity = $this->order->getTotalQtyOrdered();
            } else {
                $msg = 'DPD Error: Label type: '.$params['type'];
                $msg .= ' MPS Type: '.$mpsType;
                $msg .= ' And I don`t know what to do.';
                Mage::helper('dpd/data')->log($msg);
                $quantity = '1';
            }
        }

        return $quantity;
    }

    /**
     * @param $result
     * @return bool|mixed|null
     */
    protected function parseResult($result)
    {
        $data = null;

        switch (Mage::helper('dpd/datatype')->gettype($result)) {
            case 'json':
                $data = json_decode($result);
                if (isset($data->status) && ((string)$data->status) == 'err') {
                    Mage::helper('dpd')->registerError($data->errlog);
                    return false;
                } elseif (isset($data->status) && ((string)$data->status) == 'ok') {
                    return $data->pl_number;
                }
                return true;
            break;
            case 'serialize':
                $data = unserialize($result);
            break;
            case 'html':
                $doc = new DOMDocument();
                $doc->loadHTML($result);
                $divs = $doc->documentElement->getElementsByTagName('div');
                $h1 = $doc->documentElement->getElementsByTagName('h1');
                if ($divs != null) {
                    foreach ($divs as $div) {
                        $string = explode('.', $div->textContent);
                        if (stripos(reset($string), 'err') !== false) { //if not false err is found
                            $string = trim(str_replace(array(reset($string), '.'), '', $div->textContent));
                            Mage::helper('dpd')->registerError($string);
                            return false;
                        }
                        if (stripos($result, 'Reference') !== false) {
                            preg_match('/Reference=(.*?)=/i', $result, $match);
                            $reference = array_reverse($match);
                            $message = __('Reference: ').reset($reference).' '.ltrim($result, reset($match));
                            Mage::helper('dpd')->registerSuccess($message);
                            return true;
                        }
                        Mage::helper('dpd')->registerSuccess($div->textContent);
                        return true;
                    }
                } else {
                    Mage::helper('dpd')->registerWarning($result);
                    return false;
                }
            break;
            case 'string':
                // $doc = new DOMDocument();
                // $doc->loadHTML($result);
                // $divs = $doc->documentElement->getElementsByTagName('div');

                // foreach ($divs as $div) {
                //     echo $div->nodeValue;
                // }

            break;
            case 'file':
                return $result;
            break;
        }

        return $data;
    }
}
