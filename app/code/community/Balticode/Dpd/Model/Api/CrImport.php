<?php

/**
 * This interface creates collection requests.
 * The package is collected from client and sent to the customer.
 */
class Balticode_Dpd_Model_Api_CrImport extends Balticode_Dpd_Model_Api_Abstract
{
    /**
     * @var string
     */
    protected $interface = 'cr_import.php';

    /**
     * @var array
     */
    protected $parameters = array(
        'username' => '', //* client's weblabel username
        'password' => '', //* client's weblabel password
        'cname' => '', //* pickup name
        'cname1' => '', // pickup name1
        'cname2' => '', // pickup name2
        'cname3' => '', // pickup name3
        'cstreet' => '', //* pickup street
        'ccountry' => '', //* pickup country
        'cpostal' => '', //* pickup postal
        'ccity' => '', //* pickup city
        'cphone' => '', // pickup phone
        'cemail' => '', // pickup email
        'info1' => '', // Information field one.
        'info2' => '', // Information field two.
        'rname' => '', //* Delivery name
        'rname2' => '', // Delivery name2
        'rstreet' => '', //* Delivery street
        'rcountry' => '', //* Delivery country
        'rpostal' => '', //* Delivery postal
        'rcity' => '', //* Delivery city
        'rphone' => '', // Delivery phone
        'remail' => '' // Delivery email
    );

    /**
     * @return $this
     */
    protected function fillParameters()
    {
        parent::fillParameters(); //Fill with user and password
        $helper = Mage::helper('dpd');
        $shippingAddress = $this->order->getShippingAddress();

        $this->parameters['cname'] = $shippingAddress->getFirstname();
        $this->parameters['cname1'] = $shippingAddress->getMiddlename();
        $this->parameters['cname2'] = $shippingAddress->getLastname();
        $this->parameters['cstreet'] = $shippingAddress->getData('street');
        $this->parameters['ccountry'] = $shippingAddress->getCountryId();
        $this->parameters['cpostal'] = $shippingAddress->getPostcode();
        $this->parameters['ccity'] = $shippingAddress->getCity();
        $this->parameters['cphone'] = $shippingAddress->getTelephone();
        $this->parameters['cemail'] = $shippingAddress->getEmail();
        $this->parameters['rname'] = $helper->getConfigData('pickup_address_name');
        $this->parameters['rstreet'] = $helper->getConfigData('pickup_address_street');
        $this->parameters['rcountry'] = $helper->getConfigData('pickup_address_country');
        $this->parameters['rpostal'] = $helper->getConfigData('pickup_address_zip');
        $this->parameters['rcity'] = $helper->getConfigData('pickup_address_city');
        $this->parameters['rphone'] = $helper->getConfigData('pickup_address_phone');

        return $this;
    }

    /**
     * @param $result
     * @return bool|mixed|null
     */
    protected function parseResult($result)
    {
        $data = null;

        switch (Mage::helper('dpd/datatype')->gettype($result)) {
            case 'json':
                $data = json_decode($result);
                if (isset($data->status) && ((string)$data->status) == 'err') {
                    Mage::helper('dpd')->registerError($data->errlog);
                    return false;
                } elseif (isset($data->status) && ((string)$data->status) == 'ok') {
                    return $data->pl_number;
                }
                return true;
            break;
            case 'html':
                $doc = new DOMDocument();
                $doc->loadHTML($result);
                $divs = $doc->documentElement->getElementsByTagName('div');
                $h1 = $doc->documentElement->getElementsByTagName('h1');
                if ($divs != null) {
                    foreach ($divs as $div) {
                        $string = explode('.', $div->textContent);
                        if (stripos(reset($string), 'err') !== false) { //if not false err is found
                            $string = trim(str_replace(array(reset($string), '.'), '', $div->textContent));
                            Mage::helper('dpd')->registerError($string);
                            return false;
                        }
                        if (stripos($result, 'Reference') !== false) {
                            preg_match('/Reference=(.*?)=/i', $result, $match);
                            $reference = array_reverse($match);
                            $message = __('Reference: ').reset($reference).' '.ltrim($result, reset($match));
                            Mage::helper('dpd')->registerSuccess($message);
                            return true;
                        }
                        Mage::helper('dpd')->registerSuccess($div->textContent);
                        return true;
                    }
                } else {
                    Mage::helper('dpd')->registerWarning($result);
                    return false;
                }
            break;
        }

        return $data;
    }
}
