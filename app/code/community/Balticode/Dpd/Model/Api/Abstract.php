<?php

class Balticode_Dpd_Model_Api_Abstract extends Balticode_Dpd_Model_Api
{
    /**
     * @var string
     */
    protected $interface = 'parcel_import.php';

    /**
     * @var string
     */
    protected $path = 'parcel_interface';

    /**
     * @var array
     */
    protected $_params = array(); //some additional data

    /**
     * @var string
     */
    protected $datetime = '';

    /**
     * @var array
     */
    protected $parameters = array(
        'username' => '', //* client's weblabel username
        'password' => '' //* client's weblabel password
    );

    /**
     * @var array
     */
    protected $dataToSend = array();

    /**
     * Array of orders objects
     * @var array
     */
    protected $orders = array();

    /**
     * Balticode_Dpd_Model_Api_Abstract constructor.
     *
     * @param null $orders
     */
    public function __construct($orders = null)
    {
        if ($orders) {
            $this->collectOrders($orders);
        }
    }

    /**
     * @param $date
     */
    public function setDateTime($date)
    {
        $this->datetime = $date;
    }

    /**
     * @param $order
     * @return $this
     */
    public function setOrder($order)
    {
        if (is_numeric($order)) {
            $order = Mage::getModel('sales/order')->load($order);
        }

        if (is_object($order)) {
            $this->orders[$order->getEntityId()] = $order;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param $order
     * @return $this
     */
    public function removeOrder($order)
    {
        if (is_object($order)) {
            $order = $order->getEntityId();
        }

        if (isset($this->orders[$order])) {
            unset($this->orders[$order]);
        }

        return $this;
    }

    /**
     * @param $orders
     * @return $this
     */
    public function collectOrders($orders)
    {
        if ($orders) { // else session has been lost
            if (is_numeric($orders)) {
                $this->setOrder($orders);
            } else {
                foreach ($orders as $order) {
                    $this->setOrder($order);
                }
            }
        }

        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function collectData($params = array())
    {
        $this->_params = $params;
        if (count($this->orders)) {
            foreach ($this->orders as $currentOrder) {
                $this->order = $currentOrder;
                $this->dataToSend[] = $this->fillParameters();
            }
        } else {
            $this->dataToSend[] = $this->fillParameters();
        }

        unset($this->order);

        return $this;
    }

    /**
     * @param bool $parse
     * @return array
     */
    public function sendCollection($parse = true)
    {
        $result = array();
        foreach ($this->orders as $order) {
            $this->order = $order;
            $this->fillParameters();
            $result[] = $this->send($parse);
        }

        return $result;
    }

    /**
     * @param bool $parse
     * @return bool|mixed|mixed|null
     */
    public function send($parse = true)
    {
        $store_id = $this->getStoreIdByOrder($this->orders);

        //$api = Mage::getModel('dpd/api');
        $apiReturn = $this->collectLogins($store_id);

        if ($apiReturn === false) {
            foreach ($this->getErrorMessages() as $errorMessage) {
                Mage::getSingleton('adminhtml/session')->addError($errorMessage);
            }
            return false;
        }

        $result = $this->postData($this->parameters, $this->formatUrl());

        if ($parse) {
            $result = $this->parseResult($result);
        }
        return $result;
    }

    /**
     * @return $this
     */
    protected function fillParameters()
    {
        $store_id = $this->getStoreIdByOrder($this->orders);

        if (!count($store_id)) {
            if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) {
                $store_id = Mage::getModel('core/store')->load($code)->getId();
            } elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) {
                $website_id = Mage::getModel('core/website')->load($code)->getId();
                $store_id = Mage::app()->getWebsite($website_id)->getDefaultStore()->getId();
            } else {
                $store_id = 0;
            }
        }

        $this->collectLogins($store_id);
        if($this->datetime !== '') {
            $this->parameters['date'] = $this->datetime;
        }

        $this->parameters['username'] = $this->api_name;
        $this->parameters['password'] = $this->api_pass;

        return $this;
    }

    /**
     * @return string
     */
    protected function formatUrl()
    {
        $hostName = rtrim(Mage::helper('dpd')->getConfigData('api'), '/').'/';
        $hostName .= str_replace('//', '/', $this->path.'/'.$this->interface);

        return $hostName;
    }

    /**
     * @param $result
     * @return bool|mixed|null
     */
    protected function parseResult($result)
    {
        $data = null;

        switch (Mage::helper('dpd/datatype')->gettype($result)) {
            case 'json':
                $data = json_decode($result);
                if (isset($data->status) && ((string)$data->status) == 'err') {
                    Mage::helper('dpd')->registerError($data->errlog);
                    return false;
                } elseif (isset($data->status) && ((string)$data->status) == 'ok') {
                    if (isset($data->parcels)) {
                        return $data->parcels;
                    } elseif (isset($data->pl_number)) {
                        return $data->pl_number;
                    } elseif (isset($data->parcelshops)) {
                        return $data->parcelshops;
                    } else {
                        return $data;
                    }
                }
                return true;
            break;
            case 'serialize':
                $data = unserialize($result);
            break;
            case 'html':
                $doc = new DOMDocument();
                $doc->loadHTML($result);
                $divs = $doc->documentElement->getElementsByTagName('div');
                $h1 = $doc->documentElement->getElementsByTagName('h1');
                if ($divs != null) {
                    foreach ($divs as $div) {
                        $string = explode('.', $div->textContent);
                        if (stripos(reset($string), 'err') !== false) { //if not false err is found
                            $string = trim(str_replace(array(reset($string), '.'), '', $div->textContent));
                            Mage::helper('dpd')->registerError($string);
                            return false;
                        }
                        if (stripos($result, 'Reference') !== false) {
                            preg_match('/Reference=(.*?)=/i', $result, $match);
                            $reference = array_reverse($match);
                            $message = __('Reference: ').reset($reference).' '.ltrim($result, reset($match));
                            Mage::helper('dpd')->registerSuccess($message);
                            return true;
                        }
                        Mage::helper('dpd')->registerSuccess($div->textContent);
                        return true;
                    }
                //} elseif($h1 != null) {
                //    Mage::helper('dpd')->registerError($h1);
                } else {
                    Mage::helper('dpd')->registerWarning($result);
                    return false;
                }
            break;
            case 'string':
                // $doc = new DOMDocument();
                // $doc->loadHTML($result);
                // $divs = $doc->documentElement->getElementsByTagName('div');

                // foreach ($divs as $div) {
                //     echo $div->nodeValue;
                // }

            break;
            case 'file':
                return $result;
            break;
            default:
                # code...
                break;
        }

        return $data;
    }

    /**
     * @param $orders
     * @return array
     */
    private function getStoreIdByOrder($orders)
    {
        $store_ids = array();
        foreach ($orders as $order) {
            $store_ids[] = $order->getStoreId();
        }

        $store_ids = array_unique($store_ids);

        return $store_ids;
    }
}
