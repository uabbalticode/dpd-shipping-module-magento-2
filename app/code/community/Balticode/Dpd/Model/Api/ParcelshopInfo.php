<?php

class Balticode_Dpd_Model_Api_ParcelshopInfo extends Balticode_Dpd_Model_Api_Abstract
{
    /**
     * @var string
     */
    protected $interface = 'parcelshop_info.php';

    /**
     * @param $result
     * @return mixed
     */
    protected function parseResult($result)
    {
        return $result;
    }
}
