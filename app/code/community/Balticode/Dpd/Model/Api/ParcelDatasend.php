<?php

class Balticode_Dpd_Model_Api_ParcelDatasend extends Balticode_Dpd_Model_Api_Abstract
{
    /**
     * @var string
     */
    protected $interface = 'parcel_datasend.php';

    /**
     * @param $result
     * @return bool
     */
    protected function parseResult($result)
    {
        $result = parent::parseResult($result);
        if (is_string($result)) {
            return true;
        }
    }
}
