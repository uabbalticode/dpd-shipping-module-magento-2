<?php

class Balticode_Dpd_Model_Api_Labels extends Balticode_Dpd_Model_Api_Abstract
{
    /**
     * @var string
     */
    protected $interface = 'parcel_print.php';

    /**
     * @var array
     */
    protected $barcodes = array();

    /**
     * @param $barcodes
     * @return bool|mixed|null
     */
    public function getPDF($barcodes)
    {
        $this->barcodes = $barcodes;
        $this->collectData();
        $pdfContent = $this->send();
        return $pdfContent;
    }

    protected function fillParameters()
    {
        parent::fillParameters(); //Fill with user and password
        $this->parameters['parcels'] = implode('|', $this->barcodes);
    }

    /**
     * @param $result
     * @return bool|mixed|null
     */
    protected function parseResult($result)
    {
        $data = null;

        switch (Mage::helper('dpd/datatype')->gettype($result)) {
            case 'json':
                $data = json_decode($result);
                if (isset($data->status) && ((string)$data->status) == 'err') {
                    Mage::helper('dpd')->registerError($data->errlog);
                    return false;
                } elseif (isset($data->status) && ((string)$data->status) == 'ok') {
                    return $data;
                }
                return true;
            break;
            case 'serialize':
                $data = unserialize($result);
            break;
            case 'html':
                $doc = new DOMDocument();
                $doc->loadHTML($result);
                $divs = $doc->documentElement->getElementsByTagName('div');
                $h1 = $doc->documentElement->getElementsByTagName('h1');
                if ($divs != null) {
                    foreach ($divs as $div) {
                        $string = explode('.', $div->textContent);
                        if (stripos(reset($string), 'err') !== false) { //if not false err is found
                            $string = trim(str_replace(array(reset($string), '.'), '', $div->textContent));
                            Mage::helper('dpd')->registerError($string);
                            return false;
                        }
                        if (stripos($result, 'Reference') !== false) {
                            preg_match('/Reference=(.*?)=/i', $result, $match);
                            $reference = array_reverse($match);
                            $message = __('Reference: ').reset($reference).' '.ltrim($result, reset($match));
                            Mage::helper('dpd')->registerSuccess($message);
                            return true;
                        }
                        Mage::helper('dpd')->registerSuccess($div->textContent);
                        return true;
                    }
                //} elseif($h1 != null) {
                //    Mage::helper('dpd')->registerError($h1);
                } else {
                    Mage::helper('dpd')->registerWarning($result);
                    return false;
                }
            break;
            case 'string':
                // $doc = new DOMDocument();
                // $doc->loadHTML($result);
                // $divs = $doc->documentElement->getElementsByTagName('div');

                // foreach ($divs as $div) {
                //     echo $div->nodeValue;
                // }

            break;
            case 'file':
                return $result;
            break;
            default:
                # code...
                break;
        }

        return $data;
    }

    /**
     * Group orders who not have Barcodes
     * This is need for MPS method
     *
     * @param array $id_orders
     * @param       $action
     * @return array|bool
     *
     * id_orders - Order ids who need group
     * action - Multidimensional array
     *         array(
     *             'single' => array([orders]), //Single order not grouping
     *             'multi' => array(
     *                  'groups'=> array([order])), //Grouped orders
     *             'given' => array([orders]), //order list who already have barcode in messages
     *         )
     */
    public function groupOrders($id_orders = array(), $action)
    {
        if (!is_array($id_orders)) { //if not array set
            return false;
        }
        //MPS grouping data
        // MPS available when:
        //  Same Shipping Address;
        //  Same Package Type;
        //  Same Delivery Day;
        //  Order Delivery NOT to ParcelStore

        $groupedOrders = array(
            'single' => array(),
            'given' => array(),
            'multi' => array(),
            'mps' => array()
        );

        $orders = array();
        if (!count($id_orders)) { //if orders id is empty
            return $groupedOrders; //return empty array
        }

        foreach ($id_orders as $id_order) {
            if (Mage::getModel('dpd/carrier')->hasBarcode($id_order)) { //This order already has barcode?
                $groupedOrders['given'][] = $id_order;
                continue;
            }
            $orderData = Mage::getModel('sales/order')->load($id_order);
            $customerAddressId = $orderData->getShippingAddress()->getCustomerAddressId();
            if (empty($customerAddressId)) {
                $customerAddressId = $orderData->getBillingAddress()->getCustomerAddressId();
            }
            if (empty($customerAddressId)) {
                $customerAddressId = $orderData->getShippingAddressId();
            }

            $orders[$customerAddressId][$this->getParcelType($id_order)][] = array(
                'order' => $id_order,
                'shipping_address' => $customerAddressId, //same shipping address
                'shipping_type' => $this->getParcelType($id_order), //same shipping type
            );
        }

        if ($action == 'split') { //We need to split order by item to create MPS
            foreach ($orders as $customer_address_id => $data_delivery) {
                foreach ($data_delivery as $type_order => $data_order) {
                    foreach ($data_order as $order) {
                        if ($type_order == 'PS') {
                            $groupedOrders['single'][$customer_address_id][$type_order][] = $order['order'];
                        } else {
                            $groupedOrders['mps'][$customer_address_id][$type_order][] = $order['order'];
                        }
                    }
                }
            }
        }

        if ($action == 'join') {//We need to join order to create one label
            foreach ($orders as $customer_address_id => $data_delivery) {
                foreach ($data_delivery as $type_order => $data_order) {
                    if (count($data_order) > 1) {
                        foreach ($data_order as $order) {
                            if ($type_order == 'PS') {
                                $groupedOrders['single'][$customer_address_id][$type_order][] = $order['order'];
                            } else {
                                $groupedOrders['multi'][$customer_address_id][$type_order][] = $order['order'];
                            }
                        }
                    } else {
                        $groupedOrders['single'][$customer_address_id][$type_order][] = $data_order[0]['order'];
                    }
                }
            }
        }

        return $groupedOrders;
    }
}
