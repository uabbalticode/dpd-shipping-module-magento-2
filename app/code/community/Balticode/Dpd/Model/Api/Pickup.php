<?php

class Balticode_Dpd_Model_Api_Pickup extends Balticode_Dpd_Model_Api_Abstract
{
    /**
     * @var string
     */
    protected $interface = 'pickupOrdersSave.php';

    /**
     * @var string
     */
    protected $path = 'parcel_interface/dpdis';

    /**
     * @var array
     */
    protected $parameters = array(
        'username' => '', //* client's weblabel username
        'password' => '', //* client's weblabel password
        'payerName' => '',
        'senderName' => '',
        'senderContact' => '',
        'senderAddress' => '',
        'senderPostalCode' => '',
        'senderCountry' => '',
        'senderCity' => '',
        'senderPhone' => '',
        'parcelsCount' => '',
        'palletsCount' => '',
        'nonStandard' => ''
    );

    protected function fillParameters()
    {
        parent::fillParameters(); //Fill with user and password
        $params = $this->_params;
        $this->parameters['payerName'] = Mage::helper('dpd/data')->getConfigData('pickup_address_name');
        $this->parameters['senderName'] = Mage::helper('dpd/data')->getConfigData('pickup_address_name');
        $this->parameters['senderContact'] = Mage::helper('dpd/data')->getConfigData('pickup_address_name');
        $this->parameters['senderAddress'] = Mage::helper('dpd/data')->getConfigData('pickup_address_street');
        $this->parameters['senderPostalCode'] = Mage::helper('dpd/data')->getConfigData('pickup_address_zip');
        $this->parameters['senderCountry'] = Mage::helper('dpd/data')->getConfigData('pickup_address_country');
        $this->parameters['senderCity'] = Mage::helper('dpd/data')->getConfigData('pickup_address_city');
        $this->parameters['senderPhone'] = Mage::helper('dpd/data')->getConfigData('pickup_address_phone');
        $this->parameters['parcelsCount'] = $params['Po_parcel_qty'];
        $this->parameters['palletsCount'] = $params['Po_pallet_qty'];
        $this->parameters['nonStandard'] = $params['Po_remark'];
    }

    /**
     * @param $result
     * @return bool
     */
    protected function parseResult($result)
    {
        if (strip_tags($result) == 'DONE') {
            Mage::getSingleton('adminhtml/session')->addSuccess(__('Call courier success'));
            return true;
        } else {
            Mage::getSingleton('adminhtml/session')->addError(__('Call courier error:').' '.strip_tags($result));
            return false;
        }
    }
}
