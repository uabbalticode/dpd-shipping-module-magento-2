<?php
/**
 * 2015 UAB BaltiCode
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License available
 * through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@balticode.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this module to
 * newer versions in the future.
 *
 *  @author    UAB Balticode Kęstutis Kaleckas
 *  @package   Balticode_DPD
 *  @copyright Copyright (c) 2015 UAB Balticode (http://balticode.com/)
 *  @license   http://www.gnu.org/licenses/gpl-3.0.txt  GPLv3
 */

class Balticode_Dpd_Model_Api
{
    /**
     * API User Name
     * @var string
     */
    protected $api_name = '';

    /**
     * API User Password
     * @var string
     */
    protected $api_pass = '';

    /**
     * API id
     * @var string
     */
    protected $id = '';

    /**
     * API URL
     * @var string
     */
    protected $api_url ='';

    /**
     * @var array
     */
    public $warning_messages = array();

    /**
     * @var array
     */
    public $error_messages = array();

    /**
     * @param $store_ids
     * @return $this|bool
     */
    public function collectLogins($store_ids)
    {
        if (!is_array($store_ids)) {
            $store_ids = array($store_ids);
        }

        $api_name = array();
        $api_pass = array();
        $id = array();
        $api_url = array();

        foreach ($store_ids as $store_id) {
            $api_name[] = Mage::helper('dpd/data')->getConfigData('username', $store_id);
            $api_pass[] = Mage::helper('dpd/data')->getConfigData('password', $store_id);
            $id[] = Mage::helper('dpd/data')->getConfigData('id', $store_id);
            $api_url[] = Mage::helper('dpd/data')->getConfigData('api', $store_id);
        }

        $api_name = array_unique($api_name);
        $api_pass = array_unique($api_pass);
        $id = array_unique($id);
        $api_url = array_unique($api_url);
        $store_id = null;
        if (count($api_name) <= 1
            && count($api_pass) <= 1
            && count($id) <= 1
            && count($api_url) <= 1
        ) {
            $this->api_name = reset($api_name);
            $this->api_pass = reset($api_pass);
            $this->id = reset($id);
            $this->api_url = reset($api_url);
        } else {
            $message = Mage::helper('dpd')->__('Selected order has difference logins');
            $this->setErrorMessage($message);
            // @todo: move to exeption
            return false;
        }

        return $this;
    }

    /**
     * Ask API about available parcelStore list
     * Available filter of country or city
     *
     * @param  boolean | string $country if boolean - false filter is not enabled
     *                                   if String - filter value
     * @param  boolean | string $city    if boolean - false filter is not enabled
     *                                   if String - filter value
     * @return array           PacelStore list
     */
    public function getDeliveryPoints($country = false, $city = false)
    {
        $parcelShopHelper = Mage::getModel('dpd/api_ParcelshopInfo');
        $parcelShopHelper->collectData();
        $result = $parcelShopHelper->send();

        $data = json_decode($result);
        if ($data->status == 'ok') {
            $all_points = $data->parcelshops;
            $correct_points = $this->getFiltredPoints($all_points, $country, $city);
            return $correct_points;
        } else {
            $this->setErrorMessage($data->errlog);
            return $data;
        }
    }

    /**
     * Parcel Store filter
     *
     * @param  array $all_points All Available ParcelStore points collected from API
     * @param  boolean | string $country    filter attribute
     * @param  boolean | string $city       filter attribute
     * @return array             return Available ParcelStore filtreted by attributes
     */
    private function getFiltredPoints($all_points, $country, $city)
    {
        if ($country) {
            $this->setFiltringCountry($country);
            $all_points = array_filter($all_points, array($this, 'filterByCountry'));
        }
        if ($city) {
            $this->setFiltringCity($city);
            $all_points = array_filter($all_points, array($this, 'filterByCity'));
        }
        return $all_points;
    }

    /**
     * Send parameters to DPD API about who we need from them
     * If return value is not correct register like some error
     *
     * @param  array  $params Some params about who need from API
     * @param  string $url    Url from where need to get params
     *                        by default is not need to set - Grab from backend settings
     * @return mixed            some Parameters returned from DPD API
     */
    private function getResource($params = array('action' => 'parcelshop_info'), $url = null)
    {
        $api = $url_link = (($url === null)?$this->api_url:$url);
        Mage::helper('dpd')->debug($params);


        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'content' => http_build_query($params),
                'timeout' => 10,
            )
        );

        $context = stream_context_create($options);
        $contents = file_get_contents($api, false, $context);

        Mage::helper('dpd')->debug($contents);

        if ($contents === false) {
            $message = array('status' => 'err', 'errlog' => 'Wrong URL: '.$api);
            $contents = json_encode($message);
        }
        return $contents;
    }

    /**
     * Send collected parameters to DPD API
     *
     * @param  array    $parameters
     * @return mixed      some returned result from API
     */
    public function postData($parameters, $url = null)
    {
        $parameters['version'] = 'magento-'.(string) Mage::getConfig()->getNode()->modules->Balticode_Dpd->version;
        $response = $this->getResource($parameters, $url);
        // if (self::is_pdf($response)) {//Is pdf file content?
        //     return $response;
        // }

        // if (is_string($response) && self::is_Json($response)) { //Is string of jSon?
        //     $response = json_decode($response); //Convert to Object
        // }

        // if (is_object($response)) { //Is object?
        //     if ($response->status !== 'ok') //Is status ok?
        //     {
        //         $this->setErrorMessage($response->errlog);
        //         return false;
        //     }
        // }

        return $response;
    }

    /**
     * Set filter about country
     *
     * @param string    $country country name
     * @return $this
     */
    public function setFiltringCountry($country)
    {
        $this->filtring_country = $country;
        return $this;
    }

    /**
     * Set filter about city
     *
     * @param string    $city name
     * @return $this
     */
    public function setFiltringCity($city)
    {
        $this->filtring_city = $city;
        return $this;
    }

    /**
     * @param $obj
     * @return bool
     */
    private function filterByCountry($obj)
    {
        return ($obj->country == $this->filtring_country)? true : false;
    }

    /**
     * @param $obj
     * @return bool
     */
    private function filterByCity($obj)
    {
        return ($obj->city == $this->filtring_city)? true : false;
    }

    /**
     * Put Message to array like warning
     *
     * @param string $message some message about warning
     */
    private function setWarningMessage($message)
    {
        if (!is_string($message)) {
            return false;
        }
        $this->warning_messages[] = $message; //Put message to array
        Mage::helper('dpd/data')->log($message); //Put same message and in log file
    }

    /**
     * Return array of warning messages
     *
     * @param  boolean $clear Do clean array after read all messages?
     * @return array of warnings
     */
    public function getWarningMessages($clear = true)
    {
        $messages = $this->warning_messages;
        if ($clear) {
            $this->warning_messages = array();
        }
        return $messages;
    }

    /**
     * Put Message to array like error
     *
     * @param string $message some message about error
     */
    public function setErrorMessage($message)
    {
        if (!is_string($message)) {
            return false;
        }
        $this->error_messages[] = $message; //Put message to array
        Mage::helper('dpd/data')->log($message); //Put same message and in log file
    }

    /**
     * Return array of error messages
     *
     * @param  boolean $clear Do clean array after read all messages?
     * @return array of errors
     */
    public function getErrorMessages($clear = true)
    {
        $messages = array_unique($this->error_messages);
        if ($clear) {
            $this->error_messages = array();
        }
        return $messages;
    }
}
