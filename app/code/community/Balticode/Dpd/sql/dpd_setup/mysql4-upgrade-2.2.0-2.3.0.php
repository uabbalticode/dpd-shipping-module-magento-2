<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()
->addColumn($installer->getTable('dpd/deliverypoints'), 'cod', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_BOOLEAN,
    'nullable'  => false,
    'default'   => 0,
    'comment'   => 'COD is allowed'
    ));
$installer->endSetup();
