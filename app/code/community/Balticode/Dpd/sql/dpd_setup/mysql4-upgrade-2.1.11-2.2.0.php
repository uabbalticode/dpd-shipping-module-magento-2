<?php

$installer = $this;

$installer->startSetup();

/**
 * SQL Table for delivery points
 * Data from DPD API about Parcel Store places
 */
$installer->run("
    ALTER TABLE {$this->getTable('balticode_dpd_delivery_point')}
    MODIFY COLUMN `parcelshop_id` varchar(11);
    ");

$installer->endSetup();
